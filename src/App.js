import './App.css';
import {useState} from "react";
import {nanoid} from "nanoid";
import TriesCounter from "./components/TriesCounter/TriesCounter";
import WinMessage from "./components/WinMessage/WinMessage";
import GameDesc from "./components/GameDesc/GameDesc";
import ResetButton from "./components/ResetButton/ResetButton";

function App() {
  const [cells, setCells] = useState([]);
  const [triesCount, setTriesCount] = useState(0);
  const [ringFound, setRingFound] = useState(false);

  const createCells = () => {
      setCells([]);
      setTriesCount(0);
      setRingFound(false);
    const ringCellIndex = Math.floor(Math.random() * 36);

    for(let i = 0; i < 36; i++) {
      const hasItem = (ringCellIndex === i);
      setCells(prev => [...prev, {
          key: nanoid(), id: nanoid(), isOpened: false, hasItem: hasItem,
      }]);
    }
  }

  let message = '';

  if (ringFound) {
      message = <WinMessage />;
  }

  const chooseCell = id => {
      setCells(cells.map(cell => {
          if (cell.id === id && !cell.isOpened && !ringFound) {
              setTriesCount(triesCount+1);
              if (cell.hasItem) {
                  setRingFound(true);
              }
              return {
                  ...cell,
                  isOpened: true,
              };
          }
          return cell;
      }));
  }

  return (
    <div className="App container">
        {message}
        <GameDesc cells={cells} clickCell={chooseCell} />
        <TriesCounter triesCount={triesCount} />
        <ResetButton resetDesc={createCells} />
    </div>
  );
}

export default App;
