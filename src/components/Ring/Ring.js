import React from 'react';
import './Ring.css';

const Ring = () => {
    return (
        <span className="ring">
            o
        </span>
    );
};

export default Ring;