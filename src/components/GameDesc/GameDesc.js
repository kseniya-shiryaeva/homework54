import React from 'react';
import Cell from "../Cell/Cell";

const GameDesc = ({cells, clickCell}) => {
    return (
        <div className="game-container">
            {cells.map(item => (
                <Cell id={item.id} hasItem={item.hasItem} isOpened={item.isOpened} key={item.key} onClickCell={clickCell} />
            ))}
        </div>
    );
};

export default GameDesc;