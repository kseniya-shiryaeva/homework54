import React from 'react';

const ResetButton = ({resetDesc}) => {
    return (
        <div>
            <button type="button" onClick={resetDesc}>Reset</button>
        </div>
    );
};

export default ResetButton;