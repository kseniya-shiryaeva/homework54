import React from 'react';
import './WinMessage.css';

const WinMessage = () => {
    return (
        <div className="win-message">
            You are win!
        </div>
    );
};

export default WinMessage;