import React from 'react';

const TriesCounter = ({triesCount}) => {
    return (
        <div>
            <p className="counter">
                Tries: {triesCount}
            </p>
        </div>
    );
};

export default TriesCounter;