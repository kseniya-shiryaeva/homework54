import React from 'react';
import './Cell.css';
import Ring from "../Ring/Ring";

const Cell = ({id, isOpened, hasItem, onClickCell}) => {
    const cellClasses = ['cell'];

    if (isOpened) {
        cellClasses.push('opened');
    }

    let item = '';

    if (hasItem && isOpened) {
        item = <Ring />;
    }

    return (
        <div className={cellClasses.join(' ')} id={id} onClick={() => onClickCell(id)}>
            {item}
        </div>
    );
};

export default Cell;